const fs = require('fs');
const path = require('path');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router(path.join(__dirname, 'db.json'));
const middlewares = jsonServer.defaults();
const db = JSON.parse(fs.readFileSync('./db.json', 'UTF-8'));

const SECRET_KEY = '123456789';
const expiresIn = '1h';

server.use(middlewares);
server.use(bodyParser.json());


// Create a token from a payload 
function createToken(payload) {
    return jwt.sign(payload, SECRET_KEY, { expiresIn })
}

// Verify the token 
function verifyToken(token) {
    return jwt.verify(token, SECRET_KEY, (err, decode) => decode !== undefined ? decode : err)
}

// Check if the user exists in database
function isAuthenticated({ email, password }) {
    return db.users.find(user => user.email === email && user.password === password)
}

server.post('/auth/register', (req, res) => {
    let nextUserId = 0;
    if (db.users.length === 0) {
        nextUserId = 1;
    } else {
        nextUserId = db.users[db.users.length - 1].id + 1;
    }
    const userDetails = req.body;
    if ( typeof userDetails.name === 'undefined' || typeof userDetails.email === 'undefined' ||
        typeof userDetails.password === 'undefined' || userDetails.phone === 'undefined') {
            const status = 401;
            const message = 'All the fields are mandatory';
            res.status(status).json({ status, message });
            return;
    }

    if (userDetails.name.length <= 4) {
        const status = 401;
        const message = 'User name should have more than 4 characters';
        res.status(status).json({ status, message });
        return;
    }

    if (db.users.findIndex(user => user.email === userDetails.email) !== -1) {
        const status = 401;
        const message = 'User already exists';
        res.status(status).json({ status, message });
        return;
    }

    userDetails.id = nextUserId;
    userDetails.address = []
    db.users.push(userDetails);
    fs.writeFile('db.json', JSON.stringify(db, null, 4), 'utf8', function(err, data) {
        if (err) {
            const status = 503;
            const message = 'Unable to create new user. Please contact site\'s administrator.';
            res.status(status).json({ status, message });
        }
    });
    const access_token = createToken({ name: userDetails.name, email: userDetails.email });
    res.status(200).json({ access_token });
});

server.delete('/users/*', function (req, res, next) {
    res.sendStatus(403)
});

server.post('/auth/login', (req, res) => {
    const { email, password } = req.body;
    const user = isAuthenticated({ email, password });
    if ( typeof user === 'undefined') {
        const status = 401;
        const message = 'Incorrect email or password';
        res.status(status).json({ status, message });
        return;
    }
    const access_token = createToken({ name: user.name, email: user.email });
    res.status(200).json({ access_token });
})

// server.use(/^(?!\/auth).*$/, (req, res, next) => {
//     if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
//         const status = 401;
//         const message = 'Bad authorization header';
//         res.status(status).json({ status, message });
//         return;
//     }
//     try {
//         verifyToken(req.headers.authorization.split(' ')[1]);
//         next();
//     } catch (err) {
//         const status = 401;
//         const message = 'Error: access_token is not valid';
//         res.status(status).json({ status, message });
//     }
// })

server.use(router);

server.listen(3000, () => {
    console.log('JSON Server is running on port 3000');
})